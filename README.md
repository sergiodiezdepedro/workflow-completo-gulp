# Flujo de trabajo con Gulp

Este es un scaffolding para trabajar con Gulp 4 como automatizador de tareas.

- Compila Sass y Pug.
- Servidor de trabajo con refresco de página.
<!-- - Concatenación de archivos JavaScript y minificación. -->
- Minificación y optimización del archivo css para producción.

Mediante el comando `gulp watch` se lanzan las tareas básicas de desarrollo.

<!-- Mediante el comando `uncss` se eliminan del archivo css de producción las clases no utilizadas efectivamente en los archivos html.  -->